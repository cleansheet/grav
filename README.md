# GRAV Docker Image

This is a base GRAV image. Use it in conjunction with a container that contains the page files, novadin/php-fpm and novadin/nginx.

## Usage

The page files should be attached as a volume from a separate container.

```
docker run --volumes-from <app container> --name grav novadin/grav
```
